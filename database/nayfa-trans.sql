-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2017 at 05:48 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nayfa-trans`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(10) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'galang', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `berangkat`
--

CREATE TABLE `berangkat` (
  `id_berangkat` int(10) NOT NULL,
  `id_supir` int(10) NOT NULL,
  `id_mobil` int(10) NOT NULL,
  `tanggal_jemput` varchar(50) NOT NULL,
  `jam_pulang` varchar(50) NOT NULL,
  `jam_berangkat` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berangkat`
--

INSERT INTO `berangkat` (`id_berangkat`, `id_supir`, `id_mobil`, `tanggal_jemput`, `jam_pulang`, `jam_berangkat`) VALUES
(16, 1, 1, '12/01/2016', '10.00', '13.00'),
(17, 1, 1, '12/02/2016', '09.00', '14.00'),
(18, 1, 1, '12/09/2016', '09.00', '09.00'),
(19, 1, 1, '12/13/2016', '', '13.00'),
(20, 1, 1, '12/28/2016', '', '13.00'),
(21, 2, 2, '12/28/2016', '', '14.00'),
(22, 2, 1, '12/30/2016', '', '13.00'),
(23, 2, 1, '12/31/2016', '', ''),
(24, 1, 1, '01/01/2017', '', '13.00'),
(25, 2, 2, '01/01/2017', '', '15.00'),
(26, 1, 1, '01/05/2017', '', '07:06'),
(27, 1, 1, '01/06/2017', '', '09:00'),
(28, 2, 2, '01/06/2017', '', '09:00'),
(29, 1, 1, '01/07/2017', '', '09:00'),
(30, 1, 1, '01/08/2017', '12:00', '09:00'),
(31, 2, 2, '01/08/2017', '13:00', '09:00');

-- --------------------------------------------------------

--
-- Table structure for table `departure_juanda`
--

CREATE TABLE `departure_juanda` (
  `id_depart` int(225) NOT NULL,
  `maskapai` varchar(225) NOT NULL,
  `flight` varchar(225) NOT NULL,
  `menuju` varchar(225) NOT NULL,
  `jam` varchar(225) NOT NULL,
  `term` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departure_juanda`
--

INSERT INTO `departure_juanda` (`id_depart`, `maskapai`, `flight`, `menuju`, `jam`, `term`, `status`) VALUES
(1, 'Citilink', 'QG806', 'JAKARTA', '21:00', 'T1', 'Final Call'),
(2, 'Air Asia', 'XT-7681', 'JAKARTA', '21:45', 'T2', 'Ke Ruang Tunggu'),
(3, 'Garuda Indonesia', 'GA-331', 'JAKARTA', '21:50', 'T2', 'No Operate'),
(4, 'CitiLink', 'QG816', 'HalimPK', '05:05', 'T1', 'Scheduled');

-- --------------------------------------------------------

--
-- Table structure for table `keuangan`
--

CREATE TABLE `keuangan` (
  `id_keuangan` int(225) NOT NULL,
  `tanggal_keuangan` date NOT NULL,
  `c_order` varchar(225) NOT NULL,
  `tagihan` bigint(225) NOT NULL,
  `uang_saku` bigint(225) NOT NULL,
  `bbm` bigint(225) NOT NULL,
  `tol` bigint(225) NOT NULL,
  `parkir` bigint(225) NOT NULL,
  `fee_driver` bigint(225) NOT NULL,
  `nama_driver` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keuangan`
--

INSERT INTO `keuangan` (`id_keuangan`, `tanggal_keuangan`, `c_order`, `tagihan`, `uang_saku`, `bbm`, `tol`, `parkir`, `fee_driver`, `nama_driver`) VALUES
(1, '2016-11-19', 'TRAVEL 03.00', 820000, 100000, 100000, 13000, 12000, 175000, 'Darmo'),
(2, '2016-11-19', 'TRAVEL 03.00', 640000, 100000, 100000, 16500, 12000, 120000, 'Junaidi'),
(3, '2016-11-20', 'MSC003', 890000, 100000, 100000, 13000, 12000, 175000, 'Junaidi'),
(4, '2016-11-20', 'MSPFY17', 820000, 100000, 100000, 13000, 12000, 175000, 'Junaidi');

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE `mobil` (
  `id_mobil` int(10) NOT NULL,
  `nama_mobil` varchar(225) NOT NULL,
  `jenis_mobil` varchar(225) NOT NULL,
  `plat_nomer` varchar(225) NOT NULL,
  `jum_seat` int(11) DEFAULT NULL,
  `jum_duduk` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobil`
--

INSERT INTO `mobil` (`id_mobil`, `nama_mobil`, `jenis_mobil`, `plat_nomer`, `jum_seat`, `jum_duduk`) VALUES
(1, 'Avanza', 'Van', 'N1234MM', 3, 6),
(2, 'Xenia', 'Van', 'N2233ZX', 3, 6),
(3, 'Mobilio', 'Van', 'N4551ZX', 3, 6),
(4, 'Kijang Innova', 'Van', 'N3245XC', 3, 6),
(5, 'Ertiga', 'suzuki', 'N9000KL', 3, 6),
(6, 'Kijang Innova', 'Van', 'N4566CC', 9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `nayfa`
--

CREATE TABLE `nayfa` (
  `id_nayfa` int(10) NOT NULL,
  `nama_perusahan` varchar(225) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `no_telp` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nayfa`
--

INSERT INTO `nayfa` (`id_nayfa`, `nama_perusahan`, `alamat`, `no_telp`) VALUES
(1, 'Nayfa travel', 'Perum Griya Sampoerna Blok C3 No 3 Karangploso Malang', '081333665909 (Telkomsel)\n\n082330200031 (Telkomsel)\n\n081945144494 (XL)\n\n2B4ECBB8 (PIN BB)');

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE `operator` (
  `id_operator` int(10) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`id_operator`, `username`, `password`) VALUES
(1, 'galang', '123');

-- --------------------------------------------------------

--
-- Table structure for table `sewa`
--

CREATE TABLE `sewa` (
  `id_sewa` int(225) NOT NULL,
  `tanggal_jemput` varchar(225) NOT NULL,
  `jam_jemput` varchar(100) NOT NULL,
  `nama_sewa` varchar(225) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `alamat_jemput` varchar(225) NOT NULL,
  `alamat_antar` varchar(225) NOT NULL,
  `paket` varchar(225) NOT NULL,
  `harga_sewa` bigint(225) NOT NULL,
  `tanggal_booking` date NOT NULL,
  `jam_booking` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sewa`
--

INSERT INTO `sewa` (`id_sewa`, `tanggal_jemput`, `jam_jemput`, `nama_sewa`, `no_hp`, `alamat_jemput`, `alamat_antar`, `paket`, `harga_sewa`, `tanggal_booking`, `jam_booking`) VALUES
(1, '2017-01-05', '09:00', 'Galang AS', '3842039209381', 'cjknsdcoalnsncia', 'SURABAYA', '03', 120000, '2017-01-05', '07:00'),
(4, '01/05/2017', '09.00', 'Galang AS', '6285856767017', 'Jl Jalan yang lama', 'SURABAYA', '03', 120000, '2017-01-05', '04:00'),
(5, '01/06/2017', '08.00', 'John Doe', '6285856767017', 'Jln Nyasar', 'Gubeng', '03', 120000, '2017-01-05', '05:00'),
(6, '01/05/2017', '09.00', 'Heru', '3432542312', 'Jln gasek', 'Gubeng', '03', 120000, '2017-01-05', '05:00'),
(7, '01/07/2017', '09.00', 'Galang AS', '28391802938120', 'Jln gasek', 'SURABAYA', '03', 120000, '2017-01-07', '08:00'),
(8, '01/08/2017', '09.00', 'Irfan', '2378912937129', 'Jln gasek', 'SURABAYA', '03', 120000, '2017-01-08', '04:00');

-- --------------------------------------------------------

--
-- Table structure for table `sewa_berangkat`
--

CREATE TABLE `sewa_berangkat` (
  `id_sewaberangkat` int(11) NOT NULL,
  `id_sewa` int(11) NOT NULL,
  `id_berangkat` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sewa_berangkat`
--

INSERT INTO `sewa_berangkat` (`id_sewaberangkat`, `id_sewa`, `id_berangkat`, `status`, `keterangan`) VALUES
(1, 4, 26, '1', ''),
(2, 6, 26, '1', ''),
(3, 5, 27, '1', ''),
(4, 7, 29, '1', 'pulang'),
(5, 8, 30, '1', 'pergi');

-- --------------------------------------------------------

--
-- Table structure for table `supir`
--

CREATE TABLE `supir` (
  `id_supir` int(10) NOT NULL,
  `ktp` varchar(225) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `nohp` varchar(225) NOT NULL,
  `tanggal_lahir` varchar(225) NOT NULL,
  `tempat_lahir` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supir`
--

INSERT INTO `supir` (`id_supir`, `ktp`, `nama`, `alamat`, `nohp`, `tanggal_lahir`, `tempat_lahir`) VALUES
(1, '111223-456789-0000', 'agung teguh wa', 'malang', '978-787-878-878', '11/10/2016', 'kediri'),
(2, '121212-121212-1212', 'arief', '<p>kediri</p>', '454-545-454-544', '11/03/2016', 'kediri'),
(3, '121212-121212-1212', 'arief', '<p>kediri</p>', '454-545-454-544', '11/03/2016', 'kediri');

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE `travel` (
  `id_travel` int(10) NOT NULL,
  `jam_jemput` varchar(225) DEFAULT NULL,
  `tanggal_jemput` varchar(225) DEFAULT NULL,
  `nama` varchar(225) DEFAULT NULL,
  `hp` varchar(225) DEFAULT NULL,
  `alamat_jemput` text,
  `ket_antar` text,
  `rute` varchar(225) DEFAULT NULL,
  `seat` varchar(225) DEFAULT NULL,
  `jum_seat` decimal(10,0) DEFAULT NULL,
  `harga` decimal(10,0) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `jadwal_booking` varchar(225) DEFAULT NULL,
  `waktu_booking` varchar(225) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id_travel`, `jam_jemput`, `tanggal_jemput`, `nama`, `hp`, `alamat_jemput`, `ket_antar`, `rute`, `seat`, `jum_seat`, `harga`, `jenis`, `jadwal_booking`, `waktu_booking`, `keterangan`, `status`) VALUES
(6, '10.00', '12/13/2016', 'Agung teguh', '085-649-633-443', '<p>perum aston blok i3 nomor 8</p>', '<p>juanda sidoarjo</p>', 'MLG-SBY', '2', '5', '100000', 'BA', '01/12/2016', '10:17:35', 'penumpang', '1'),
(7, '09.00', '12/13/2016', 'Agung teguh', '085-649-633-443', '<p>perumahan asrikaton blok i3 nomor 8</p>', '<p>juanda surabaya</p>', 'MLG-SBY', '2', '5', '100000', 'BA', '02/12/2016', '06:37:30', 'penumpang', '1'),
(8, '12.00', '12/13/2016', 'Ayska', '087-655-261-626', '<p>Perumahan griyasanta blok i9 nomor 3</p>', '<p>juanda sby</p>', 'MLG-JND', '3', '5', '100000', 'BA', '02/12/2016', '06:50:31', 'penumpang', '1'),
(9, '09.00', '12/13/2016', 'agung twa', '085-649-633-443', '<p>perumahan asrikaton indah</p>', '<p>surabay</p>', 'MLG-SBY', '2', '3', '100000', 'BA', '06/12/2016', '17:36:14', 'penumpang', '1'),
(10, '09.00', '12/13/2016', 'ggfg', '808-978-776-543', '<p>kjkh</p>', '<p>jhjh</p>', 'MLG-SBY', '3', '2', '100000', 'BA', '06/12/2016', '21:06:00', 'penumpang', '1'),
(11, '09.00', '12/13/2016', 'ahgung', '098-777-666-666', '<p>jjhdjhsdjshjs</p>', '<p>jsdhjdssjhl</p>', 'MLG-SBY', '2', '3', '100000', 'BA', '06/12/2016', '12:58:46', 'penumpang', '1'),
(12, '12.11', '12/28/2016', 'Heru', '098-778-987-889', '<p>mdklscmc kjc jncjsdln</p>', '<p>,mclacnascascjlnascl</p>', 'MLG-SBY', '9', '12', '900000', 'BA', '28/12/2016', '11:39:43', 'penumpang', '1'),
(13, '09.00', '12/30/2016', 'Galang AS', '908-098-908-090', '<p>ascsacsacacs</p>', '<p>cascsascascsacasc</p>', 'MLG-SBY', '04', '12', '999999', 'BA', '30/12/2016', '22:11:38', 'penumpang', '1'),
(14, '09.00', '01/01/2017', 'ascsacasc', '908-923-101-391', '<p>caklnsapc;</p>', '<p>cansal</p>', 'MLG-SBY', '12', '12', '999999', 'BA', '01/01/2017', '20:17:15', 'penumpang', '1'),
(15, '15.00', '01/01/2017', 'kndvjkdfvsl', '921-301-302-132', '<p>acsbcsahkcnsascjbakscba</p>', '<p>ajnskcanbska</p>', 'MLG-SBY', '11', '13', '989890', 'BA', '01/01/2017', '21:45:23', 'penumpang', '1'),
(16, '08.00', '01/01/2017', 'gyhjyrh', '563-453-453-523', '<p>cdvfbdhfjtjdgffdvv</p>', '<p>vdshdyjyuyjftdnfbddfbesbtbrstbdfbtf</p>', 'MLG-SBY', '2', '12', '777777', 'BA', '01/01/2017', '22:10:01', 'penumpang', '1'),
(17, '02.00', '01/01/2017', 'jjjskcnaoslkca', '093-404-320-948', '<p>casfjgdgfndfbsdvdv</p>', '<p>dfghegs</p>', 'MLG-SBY', '3', '11', '888888', 'LA', '01/01/2017', '23:27:46', 'penumpang', '1'),
(18, '09.00', '01/05/2017', 'Galang AS', '898-990-890-809', '<p>cacascascsacsac</p>', '<p>sacsacsacasc</p>', 'MLG-SBY', '04', '12', '120000', 'BA', '05/01/2017', '01:20:13', 'penumpang', '1'),
(19, '09.00', '01/05/2017', 'Irfan', '989-090-090-909', '<p>cascacasc</p>', '<p>ascsacasc</p>', 'BT-JND', '05', '12', '999999', 'BA', '05/01/2017', '18:55:02', 'penumpang', '1'),
(20, '09.00', '01/06/2017', 'Galang AS', '123-254-535-435', '<p>ascvdsdsfdaC</p>', '<p>cvascgdfsFwe</p>', 'MLG-SBY', '04', '12', '878788', 'LA', '06/01/2017', '09:32:16', 'penumpang', '1'),
(21, '12.00', '01/06/2017', 'John Doe', '384-829-394-891', '<p>kasncjabcao</p>', '<p>kacmnjacnajl</p>', 'MLG-SBY', '12', '21', '984839', 'LA', '06/01/2017', '09:33:33', 'paket', '1'),
(22, '09.00', '01/07/2017', 'Pandu', '798-797-967-988', '<p>nbjgchk</p>', '<p>jbhkjb</p>', 'MLG-SBY', '1', '12', '888888', 'BA', '06/01/2017', '19:18:08', 'penumpang', '1'),
(23, '10.00', '01/07/2017', 'John Doe', '213-214-354-353', '<p>ascmnasjc</p>', '<p>aksjncsjaicb</p>', 'MLG-SBY', '12', '12', '989090', 'BA', '07/01/2017', '20:38:27', 'penumpang', '1'),
(24, '09.00', '01/08/2017', 'Galang AS', '823-019-283-190', '<p>ascdsvcsac</p>', '<p>cafegeacq</p>', 'MLG-SBY', '05', '12', '120000', 'BA', '08/01/2017', '19:44:25', 'penumpang', '1'),
(25, '12.00', '01/08/2017', 'Mr reming', '928-391-203-812', '<p>acsacnasjco</p>', '<p>ac jskcbascaocnlao</p>', 'SBY-MLG', '2', '12', '210000', 'BA', '08/01/2017', '19:45:59', 'penumpang', '1');

-- --------------------------------------------------------

--
-- Table structure for table `travel_berangkat`
--

CREATE TABLE `travel_berangkat` (
  `id_keberangkatan` int(11) NOT NULL,
  `id_travel` int(11) DEFAULT NULL,
  `id_berangkat` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `keterangan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel_berangkat`
--

INSERT INTO `travel_berangkat` (`id_keberangkatan`, `id_travel`, `id_berangkat`, `status`, `keterangan`) VALUES
(3, 14, 24, '1', ''),
(7, 15, 24, '1', ''),
(8, 16, 25, '1', ''),
(9, 18, 26, '1', ''),
(10, 19, 26, '1', ''),
(11, 20, 27, '1', ''),
(12, 21, 27, '1', ''),
(13, 22, 29, '1', 'pulang'),
(14, 23, 29, '1', 'pergi'),
(15, 24, 30, '1', 'pergi'),
(16, 25, 30, '1', 'pulang');

-- --------------------------------------------------------

--
-- Table structure for table `tujuan`
--

CREATE TABLE `tujuan` (
  `id_tujuan` int(225) NOT NULL,
  `nama_lokasi` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tujuan`
--

INSERT INTO `tujuan` (`id_tujuan`, `nama_lokasi`) VALUES
(1, 'SURABAYA'),
(2, 'BANDARA JUANDA'),
(3, 'BATU');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `berangkat`
--
ALTER TABLE `berangkat`
  ADD PRIMARY KEY (`id_berangkat`);

--
-- Indexes for table `departure_juanda`
--
ALTER TABLE `departure_juanda`
  ADD PRIMARY KEY (`id_depart`);

--
-- Indexes for table `keuangan`
--
ALTER TABLE `keuangan`
  ADD PRIMARY KEY (`id_keuangan`);

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
  ADD PRIMARY KEY (`id_mobil`);

--
-- Indexes for table `nayfa`
--
ALTER TABLE `nayfa`
  ADD PRIMARY KEY (`id_nayfa`);

--
-- Indexes for table `operator`
--
ALTER TABLE `operator`
  ADD PRIMARY KEY (`id_operator`);

--
-- Indexes for table `sewa`
--
ALTER TABLE `sewa`
  ADD PRIMARY KEY (`id_sewa`);

--
-- Indexes for table `sewa_berangkat`
--
ALTER TABLE `sewa_berangkat`
  ADD PRIMARY KEY (`id_sewaberangkat`);

--
-- Indexes for table `supir`
--
ALTER TABLE `supir`
  ADD PRIMARY KEY (`id_supir`);

--
-- Indexes for table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id_travel`);

--
-- Indexes for table `travel_berangkat`
--
ALTER TABLE `travel_berangkat`
  ADD PRIMARY KEY (`id_keberangkatan`);

--
-- Indexes for table `tujuan`
--
ALTER TABLE `tujuan`
  ADD PRIMARY KEY (`id_tujuan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `berangkat`
--
ALTER TABLE `berangkat`
  MODIFY `id_berangkat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `departure_juanda`
--
ALTER TABLE `departure_juanda`
  MODIFY `id_depart` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `keuangan`
--
ALTER TABLE `keuangan`
  MODIFY `id_keuangan` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mobil`
--
ALTER TABLE `mobil`
  MODIFY `id_mobil` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nayfa`
--
ALTER TABLE `nayfa`
  MODIFY `id_nayfa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `operator`
--
ALTER TABLE `operator`
  MODIFY `id_operator` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sewa`
--
ALTER TABLE `sewa`
  MODIFY `id_sewa` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sewa_berangkat`
--
ALTER TABLE `sewa_berangkat`
  MODIFY `id_sewaberangkat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supir`
--
ALTER TABLE `supir`
  MODIFY `id_supir` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `travel`
--
ALTER TABLE `travel`
  MODIFY `id_travel` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `travel_berangkat`
--
ALTER TABLE `travel_berangkat`
  MODIFY `id_keberangkatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tujuan`
--
ALTER TABLE `tujuan`
  MODIFY `id_tujuan` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
